if input ~= nil then
    return 
end

input = {
    input_state = {},
    prev_input_state = {}
}

input.keys = {
    toggle = ui.new_hotkey("LUA", "A", "Input manager - toggle menu", false),
    lmouse = ui.new_hotkey("LUA", "A", "Input manager - mouse left", false),
    rmouse = ui.new_hotkey("LUA", "A", "Input manager - mouse right", false)
}

input.update = function ()
    for key, handle in pairs(input.keys) do 
        input.prev_input_state[key] = input.input_state[key] or false
        input.input_state[key] = ui.get(handle)
    end
end

input.is_key_pressed = function (key)
    for _key, _ in pairs(input.input_state) do 
        if _key == key then
            return _ == true and input.prev_input_state[key] == false
        end
    end
    
    return false
end

input.is_key_down = function (key)
    for _key, _ in pairs(input.input_state) do 
        if _key == key then 
            return _
        end
    end
    
    return false
end

input.is_key_released = function (key)
    for _key, _ in pairs(input.input_state) do 
        if _key == key then 
            return _ == false and input.prev_input_state[key] == true
        end
    end
    
    return false
end