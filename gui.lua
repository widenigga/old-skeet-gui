require("gui framework")
require("gui input")

local ctx = nil

--[[

    ctx = gui.dropdown(ctx, "Pick a preset", ui.reference("Misc", "Other", "Presets"), ui.get(test_slider), false,
    { "Legit", "Rage", "HvH", "Secret", "Headshot", "Alpha", "Bravo" })

    ctx = gui.button(ctx, "!Now you can either save it", ui.get(test_slider), ui.reference("Misc", "Other", "Save config"))
    ctx = gui.slider(ctx, "And here you can adjust controls width", test_slider, 0, 300)

--]]

local function rage_tab()
    ctx, data = gui.group(ctx, "Aimbot", 239, 437)
        ctx = gui.checkbox(ctx, "Enabled", ui.reference("Rage", "Aimbot", "Enabled"))

        ctx = gui.dropdown(ctx, "Target selection", ui.reference("Rage", "Aimbot", "Target selection"), 0, false,
            { "Cycle", "Cycle (2x)", "Near crosshair", "Highest damage", "Lowest ping", "Best K/D ratio", "Best hit chance" })
        ctx = gui.multi_dropdown(ctx, "Hitbox", ui.reference("Rage", "Aimbot", "Target hitbox"), 0, false,
            { "Head", "Chest", "Stomach", "Arms", "Legs", "Feet" })

        ctx = gui.multi_dropdown(ctx, "Multi-point", ui.reference("Rage", "Aimbot", "Multi-point"), 0, false,
            { "Head", "Chest", "Stomach", "Arms", "Legs", "Feet" })

        if #ui.get(ui.reference("Rage", "Aimbot", "Multi-point")) > 0 then
            ctx = gui.slider(ctx, "Multi-point scale", ui.reference("Rage", "Aimbot", "Multi-point scale"), 24, 100, "%.0f", false)
        end

        ctx = gui.checkbox(ctx, "Dynamic multi-point", ui.reference("Rage", "Aimbot", "Dynamic multi-point"))
        ctx = gui.slider(ctx, "Stomach hitbox scale", ui.reference("Rage", "Aimbot", "Stomach hitbox scale"), 1, 100, "%.0f", false)

        ctx = gui.checkbox(ctx, "Automatic penetration", ui.reference("Rage", "Aimbot", "Automatic penetration"))
        ctx = gui.checkbox(ctx, "Automatic fire", ui.reference("Rage", "Aimbot", "Automatic fire"))
        ctx = gui.checkbox(ctx, "Silent aim", ui.reference("Rage", "Aimbot", "Silent aim"))

        if not ui.get(ui.reference("Rage", "Other", "Remove spread")) then
            ctx = gui.slider(ctx, "Minimum hit chance", ui.reference("Rage", "Aimbot", "Minimum hit chance"), 0, 100, "%.0f", false)
        end

        ctx = gui.slider(ctx, "Maximum FOV", ui.reference("Rage", "Aimbot", "Maximum FOV"), 0, 180, "%.0f", false)
        ctx = gui.slider(ctx, "Minimum damage", ui.reference("Rage", "Aimbot", "Minimum damage"), 1, 100, "%.0f", false)

        ctx = gui.checkbox(ctx, "Automatic scope", ui.reference("Rage", "Aimbot", "Automatic scope"))
        ctx = gui.checkbox(ctx, "Reduce aim step", ui.reference("Rage", "Aimbot", "Reduce aim step"))
    ctx = gui.set_cursor_pos(ctx, data.cursor_pos_x, data.cursor_pos_y)

    ctx, data = gui.group(ctx, "Other", 239, 437)
        ctx = gui.checkbox(ctx, "!Remove spread", ui.reference("Rage", "Other", "Remove spread"))
        ctx = gui.checkbox(ctx, "Remove recoil", ui.reference("Rage", "Other", "Remove recoil"))

        ctx = gui.dropdown(ctx, "Accuracy boost", ui.reference("Rage", "Other", "Accuracy boost"), 0, false,
            { "Off", "Low", "Medium", "High", "Maximum" })
        ctx = gui.multi_dropdown(ctx, "Accuracy boost options", ui.reference("Rage", "Other", "Accuracy boost options"), 0, false,
            { "Refine shot", "Extended backtrack" })
        ctx = gui.dropdown(ctx, "Quick stop", ui.reference("Rage", "Other", "Quick stop"), 0, false,
            { "Off", "On", "On + duck", "On + slow motion" })

        ctx = gui.checkbox(ctx, "Quick stop in fire", ui.reference("Rage", "Other", "Quick stop in fire"))
        ctx = gui.checkbox(ctx, "Quick peek assist", ui.reference("Rage", "Other", "Quick peek assist"))
        ctx = gui.checkbox(ctx, "Anti-aim correction", ui.reference("Rage", "Other", "Anti-aim correction"))

        ctx = gui.dropdown(ctx, "Prefer body aim", ui.reference("Rage", "Other", "Prefer body aim"), 0, false,
            { "Off", "Always on", "Fake angles", "Aggressive", "High inaccuracy" })
        
        if ui.get(ui.reference("Rage", "Other", "Prefer body aim")) ~= "Off" then
            ctx = gui.checkbox(ctx, "Delay shot on unduck", ui.reference("Rage", "Other", "Delay shot on unduck"))
        end
    ctx = gui.set_cursor_pos(ctx, data.cursor_pos_x, data.cursor_pos_y)
end

client.set_event_callback("paint", function (_)
    input.update()

    ctx = gui.form(ctx)
    ctx = gui.set_cursor_pos(ctx, 0, ctx.size_y - 16 - 147)

    ctx = gui.dropdown(ctx, "Preset", ui.reference("Misc", "Other", "Presets"), 112, true,
        { "Legit", "Rage", "HvH", "Secret", "Headshot", "Alpha", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot",
          "Gold", "Hotel", "India", "Juliet", "Kilo" })
    ctx = gui.button(ctx, "Save", 112, ui.reference("Misc", "Other", "Save config"))
    ctx = gui.button(ctx, "Load", 112, ui.reference("Misc", "Other", "Load config"))
    ctx = gui.button(ctx, "Reset", 112, ui.reference("Misc", "Other", "Reset config"))
    ctx = gui.button(ctx, "Unload", 112, ui.reference("Misc", "Other", "Unload"))

    _, ctx = gui.get_cursor_pos(ctx) -- pop the cursor pos

    local r, g, b, a = ui.get(ui.reference("MISC", "Settings", "Menu color"))
    ctx.accent_color = { r, g, b, a }

    if ctx.open or ctx.alpha > 0 then
        ctx = gui.tab(ctx, "RAGE", rage_tab)
        ctx = gui.tab(ctx, "VISUALS")
    end
end)
